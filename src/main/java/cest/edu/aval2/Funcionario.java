package cest.edu.aval2;

import java.math.BigDecimal;

public class Funcionario extends PessoaFisica {
	private String cargo;
	private BigDecimal salario;
	
	public Funcionario(String nome, String cpf, String cargo) {
		super(nome, cpf);
		this.cargo = cargo;
	}

	@Override
	public String getNome() {
		return null;
	}

	@Override
	public String getCpf() {
		return getCpf();
		
	}
	public String getCargo() {
		return cargo;
	}
	
	public BigDecimal getSalario() {
		return salario;
	}
	
	@Override
	public int getIdade() {
		return getIdade();
	}
}
