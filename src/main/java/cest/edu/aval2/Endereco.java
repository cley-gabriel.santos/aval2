package cest.edu.aval2;

public class Endereco {
	private Integer num;
	private String logradouro;
	private Cidade cidade;

	public Endereco(Integer num, String logradouro, Cidade cidade ) {
		this.num = num;
		this.logradouro = logradouro;
		this.cidade = cidade;
		
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public Cidade getCidade() {
		return cidade;
	}
	
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	
}
