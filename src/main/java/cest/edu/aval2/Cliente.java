package cest.edu.aval2;

public class Cliente extends PessoaFisica {
	private String atendimento;
	
	public Cliente(String nome, String cpf) {
		super(nome, cpf);
	}

	@Override
	public String getNome() {
		return getNome();
	}

	@Override
	public String getCpf() {
		return getCpf();
	}
	
	public String getAtendimento() {
		return atendimento;
	}
	@Override
	public int getIdade() {
		return getIdade();
	}
} 