package cest.edu.aval2;

public class Cidade extends UF {
	private String nome;
	
	
	public Cidade(String sigla) {
		super(sigla);
		
	}
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String getSigla() {
		return super.getSigla();
	}
}

