package cest.edu.aval2;

public class Contador {
	private int num = 0;
	
	private static Contador cont = new Contador();
	
		private Contador() {}
		public static Contador getInstance() {
			return cont;
		}
		
		public int ProxNumero () {
			int i = 0;
			return ++i;
		}
		
		public int getNum() {
			return num;
		}
		public void setNum(int num) {
			this.num = num;
		}
}
